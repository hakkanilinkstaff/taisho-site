"use strict";

///////////////////////////////////
const SUBMIT_BUTTON = document.querySelector(".btn-submit");
const disableFromArunavi = document.querySelector(".disableFromArunavi")
const RESET_BUTTON = document.querySelector(".btn-reset");
const CHECKBOXES = document.querySelectorAll(".checkbox-item");

const TARGET_SITES = document.querySelectorAll(".target-site");
const SELECTED_AREAS = document.querySelectorAll(".selected-areas");
const SELECTED_AREAS1 = document.querySelectorAll(".selected-areas1");
const SELECTED_AREAS2 = document.querySelectorAll(".selected-areas2");
const SELECTED_AREAS3 = document.querySelectorAll(".selected-areas3");
const SELECTED_AREAS4 = document.querySelectorAll(".selected-areas4");
const SELECTED_AREAS5 = document.querySelectorAll(".selected-areas5");
const SELECTED_AREAS6 = document.querySelectorAll(".selected-areas6");
const JOB_TYPES = document.querySelectorAll(".job-types");
const COLLAPSIBLE = document.querySelectorAll(".collapsible");

const toggleCollaps = function () {
  COLLAPSIBLE.forEach((coll) => {
    coll.addEventListener("click", function () {
      this.classList.toggle("active");
      const content = this.nextElementSibling;
      content.classList.toggle("close-collape");
    });
  });
};

toggleCollaps();

// Reset all checkedboxs
const resetCheckbox = function () {
  RESET_BUTTON.addEventListener("click", function (e) {
    e.preventDefault();

    CHECKBOXES.forEach((checkbox) => {
      if (checkbox.checked) checkbox.checked = false;
      checkbox.closest(".checkbox-container").classList.remove("checkbox-disabled");
    });
    areas.splice(0, areas.length);
    jobTypes.splice(0, jobTypes.length);
    sites.splice(0, sites.length);
    SUBMIT_BUTTON.disabled = true;
  });
};

resetCheckbox();

// Button disabled
const buttonDisabled = function () {
  if (sites.length > 0 && areas.length > 0 && jobTypes.length > 0) {
    SUBMIT_BUTTON.disabled = false;
  } else {
    SUBMIT_BUTTON.disabled = true;
  }
};

// Checkbox checcked and unchecked
const checkedCheckboxStatus = function (element, categories, checkboxArr) {
  if (element.checked==true && (element.value=="北海道・東北地方" || element.value=="関東地方" || element.value=="中部地方" || element.value=="近畿地方" || element.value=="中国・四国地方" || element.value=="九州・沖縄地方" || element.value=="アルなび" || element.value=="XXXXXX1" || element.value=="XXXXXX2" || element.value=="XXXXXX3" || element.value=="XXXXXX4" || element.value=="XXXXXX5") ) {
    if(element.value !== "アルなび" || element.value !== "XXXXXX1" || element.value !== "XXXXXX2" || element.value !== "XXXXXX3" || element.value !== "XXXXXX4" || element.value !== "XXXXXX5")
    {
      categories.forEach((category) => 
      {
        const index = checkboxArr.indexOf(category.value);
        if (index > -1) checkboxArr.splice(index, 1);
      });
      const index = checkboxArr.indexOf(element.value);
      if (index > -1) checkboxArr.splice(index, 1);
      checkboxArr.push(element.value);
    }

    categories.forEach((el) => {
      if (el !== element) {
        el.closest(".checkbox-container").classList.add("checkbox-disabled");
        el.checked = false;
      }
    });
  }
  else
  {
    var count = 0;
    var elementInArray = false;
    var notothercategoriesInArray = false;
    const index = checkboxArr.indexOf(categories[0].value);

    if(index > -1 )
    {
      elementInArray = true;
    }

    categories.forEach((category) => 
    {
      if(category.checked == true)
      {
        const index = checkboxArr.indexOf(category.value);
        if(index == -1)
        {
          notothercategoriesInArray = true;
        }
        count++;
      }
    });
    
    if(count == (categories.length-1))
    {
      categories.forEach((category) => 
      {
        const index = checkboxArr.indexOf(category.value);
        if (index > -1) checkboxArr.splice(index, 1);
      });

      checkboxArr.push(categories[0].value);

    }
    else if(count>0)
    {
      if(elementInArray==true && notothercategoriesInArray==true)
      {
        const index = checkboxArr.indexOf(categories[0].value);
        if (index > -1) checkboxArr.splice(index, 1);

        categories.forEach((item) => 
        {
          if(item.checked == true)
          {
            checkboxArr.push(item.value);
          }
        });

      }
    }

  }

  if (!element.checked) {
    categories.forEach((el) => {
      el.disabled = false;
      el.closest(".checkbox-container").classList.remove("checkbox-disabled");
    });
  }
};

// Checkbox info
const sites = [];
const areas = [];
const jobTypes = [];


//function while selecting zenkoku
const getDataFromZenkoku = function (categories, checkboxArr)
{

  categories[0].addEventListener("click", function(e)
  {
    const all_areas = document.querySelectorAll(".selected-areas1, .selected-areas2, .selected-areas3, .selected-areas4, .selected-areas5, .selected-areas6");

     if(categories[0].checked == true)
     {
       checkboxArr.splice(0, checkboxArr.length);
       checkboxArr.push("北海道・東北すべて", "関東すべて", "中部すべて", "近畿すべて", "中国・四国すべて", "九州・沖縄すべて");

       all_areas.forEach((element) =>
       {
        element.closest(".checkbox-container").classList.add("checkbox-disabled");
        element.checked = false;
       });
     }
     else
     {
      checkboxArr.splice(0, checkboxArr.length);

      all_areas.forEach((element) =>
      {
      element.disabled = false;
      element.closest(".checkbox-container").classList.remove("checkbox-disabled");
     });
    }
    buttonDisabled();
    console.log(areas, sites, jobTypes);
  });
}

const checkedCheckboxStatusForJobtypes = function (element, categories) {
  var elementArunavi = document.getElementById("Drarunavi");
  
    categories.forEach((el) => {
      if (el !== element) {
        el.closest(".checkbox-container").classList.add("checkbox-disabled");
        el.checked = false;
      }
    });

  if (!element.checked) {
    categories.forEach((el) => {
      if(elementArunavi.checked == false)
      {
        el.disabled = false;
        el.closest(".checkbox-container").classList.remove("checkbox-disabled");
      }
      else if(elementArunavi.checked == true && el.value !== "検診")
      {
        el.disabled = false;
        el.closest(".checkbox-container").classList.remove("checkbox-disabled");
      }
    });
  }
};

// Get checkbox values
const getDataFromCheckbox = function (categories, checkboxArr) {

  categories.forEach((category) => {
    category.addEventListener("click", function (e) {
      const { checked, value } = category;

      if (checked) {
        checkboxArr.push(value);
      } else {
        const index = checkboxArr.indexOf(value);
        if (index > -1) checkboxArr.splice(index, 1);
      }

      // button disabled
      buttonDisabled();

      // Checked and Unchecked
      if(category.classList.contains('job-types'))
      {
        checkedCheckboxStatusForJobtypes(category, categories);
      }
      else
      {
        checkedCheckboxStatus(category, categories, checkboxArr);
      }
      console.log(areas, sites, jobTypes);
    });
  });
};

const getDataFromCheckbox対象サイト = function (categories, checkboxArr) {
  categories.forEach((category) => {
    category.addEventListener("click", function (e) {
      const { checked, value } = category;

      if(category.value == "アルなび" && category.checked == true)
      {
        console.log(disableFromArunavi);
        disableFromArunavi.closest(".checkbox-container").classList.add("checkbox-disabled");
      }
      else
      {
        disableFromArunavi.closest(".checkbox-container").classList.remove("checkbox-disabled");      
      }

      if (checked) {
        checkboxArr.push(value);
      } else {
        const index = checkboxArr.indexOf(value);
        if (index > -1) checkboxArr.splice(index, 1);
      }

      // button disabled
      buttonDisabled();

      // Checked and Unchecked
      checkedCheckboxStatus(category, categories, checkboxArr);
      console.log(areas, sites, jobTypes);
    });
  });
};

getDataFromCheckbox対象サイト(TARGET_SITES, sites);
getDataFromCheckbox(SELECTED_AREAS1, areas);
getDataFromCheckbox(SELECTED_AREAS2, areas);
getDataFromCheckbox(SELECTED_AREAS3, areas);
getDataFromCheckbox(SELECTED_AREAS4, areas);
getDataFromCheckbox(SELECTED_AREAS5, areas);
getDataFromCheckbox(SELECTED_AREAS6, areas);
getDataFromZenkoku(SELECTED_AREAS, areas);
getDataFromCheckbox(JOB_TYPES, jobTypes);

// Onsubmit handler
SUBMIT_BUTTON.addEventListener("click", submitHandler);

 async function submitHandler(e) {
  const spinner = document.getElementById("wrapper");
  spinner.classList.add("wrapper");
  e.preventDefault();
  console.log(areas, sites, jobTypes);

  var currentdate = new Date(); 
  var datetime = currentdate.getFullYear() + "_" +  (currentdate.getMonth()+1) + "_" + currentdate.getDate() + "_" + currentdate.getHours() + "_" + currentdate.getMinutes() + "_" + currentdate.getSeconds();
  var fileName = sites[0] + "_" + jobTypes[0] + "_" + datetime + ".csv";
  
  var res =  await fetch('http://localhost:8000/scrap/arunavi?' + new URLSearchParams({
    param1:sites[0],
    param2:areas,
    param3:jobTypes[0], }), {
    method: 'GET',
    cache: 'no-cache'

    });

  spinner.classList.remove("wrapper");
  const blob = await res.blob();

  if(blob.size > 0){
  const newBlob = new Blob([blob]);
  
  const blobUrl = window.URL.createObjectURL(newBlob);
  
  const link = document.createElement('a');
  link.href = blobUrl;
  link.setAttribute('download', fileName); document.body.appendChild(link);
  link.click();
  link.parentNode.removeChild(link);
  
   window.URL.revokeObjectURL(blob);
  }
  else{
    alert("検索データが見つかりません");
  }
}
